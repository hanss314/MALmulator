TAGS = {}


def anime_action(key):
    def deco(f):
        TAGS[key] = f

    return deco


STATUSES = {
    'watching': 1,
    'completed': 2,
    'onhold': 3,
    'dropped': 4,
    'plantowatch': 6
}


@anime_action('episode')
def episode(d, value):
    if value is None:
        value = 0
    else:
        value = int(value)
    d['add_anime[num_watched_episodes]'] = value


@anime_action('status')
def status(d, value):
    if value is None:
        value = 6
    value = STATUSES.get(value, int(value))
    d['status'] = value


@anime_action('score')
def score(d, value):
    if value is not None:
        value = int(value)
    d['add_anime[score]'] = value


@anime_action('storage_type')
def storage_type(d, value):
    if value is not None:
        value = int(value)
    d['add_anime[storage_type]'] = value


@anime_action('storage_value')
def storage_value(d, value):
    if value is None:
        value = 0
    else:
        value = float(value)
    d['add_anime[storage_value]'] = value


@anime_action('times_rewatched')
def rewatch_count(d, value):
    if value is None:
        value = 0
    else:
        value = int(value)
    d['add_anime[num_watched_times]'] = value


@anime_action('rewatch_value')
def rewatch_value(d, value):
    if value is not None:
        value = int(value)
    d['add_anime[rewatch_value]'] = value


@anime_action('date_start')
def date_start(di, value):
    m = d = y = None
    if value is not None:
        m = int(value[0:2])
        d = int(value[2:4])
        y = int(value[4:8])

    di['add_anime[start_date][day]'] = d
    di['add_anime[start_date][month]'] = m
    di['add_anime[start_date][year]'] = y


@anime_action('date_finish')
def date_finish(di, value):
    m = d = y = None
    if value is not None:
        m = int(value[0:2])
        d = int(value[2:4])
        y = int(value[4:8])

    di['add_anime[finish_date][day]'] = d
    di['add_anime[finish_date][month]'] = m
    di['add_anime[finish_date][year]'] = y


@anime_action('priority')
def priority(d, value):
    if value is None:
        value = 0
    else:
        value = int(value)
    d['add_anime[priority]'] = value


@anime_action('enable_discussion')
def enable_discussion(d, value):
    if value is None:
        value = 0
    else:
        value = int(value)
    d['add_anime[is_asked_to_discuss]'] = value


# I am unsure about this next one. It seemed to be the only fields left.
@anime_action('enable_rewatching')
def enable_rewatching(d, value):
    if value is None:
        value = 0
    else:
        value = int(value)
    d['add_anime[sns_post_type]'] = value


@anime_action('comments')
def comments(d, value):
    d['add_anime[comments]'] = value


@anime_action('tags')
def tags(d, value):
    d['add_anime[tags]'] = value
