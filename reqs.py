import urllib.parse
import socket
import zlib
import ssl

from flask import session


class HTTPResponse:
    def __init__(self, response, headers, body):
        self.body = body
        self.status = response[1]
        self.status_code = response[0]
        self.headers = headers

        if headers.get('content-encoding', '') == 'gzip':
            self.text = zlib.decompress(body, 16 + zlib.MAX_WBITS).decode()
        else:
            self.text = body.decode()

    def __repr__(self):
        return f'<HTTPResponse {self.status_code} ({len(self.body)} bytes)>'


class HTTPHeaders:
    def __init__(self, pairs):
        self.pairs = [*pairs]

    def get(self, item, default=None):
        rtn = [i[1] for i in self.pairs if i[0].lower() == item.lower()]

        if not rtn:
            return default

        if len(rtn) == 1:
            return rtn[0]
        return rtn

    def __getitem__(self, item):
        return self.get(item)

    def __repr__(self):
        return '<HTTPHeaders ' + '; '.join(f"'{k}: {v}'" for k, v in self.pairs) + '>'

    def __contains__(self, item):
        return any(i[0].lower() == item.lower() for i in self.pairs)


def set_cookie(header):
    if not isinstance(header, list):
        header = [header]

    for i in header:
        for j in i.split('Only, '):
            j = j.split(';', 1)[0]
            session['cookies'][j.split('=', 1)[0]] = j.split('=', 1)[1]
            print(f'SET {j.split("=", 1)[0]} TO {j.split("=", 1)[1]}')


def make_request(method, endpoint, json=None, data=None, headers=None, cookies=None):
    if 'cookies' not in session:
        session['cookies'] = {}
        session['cookies']['is_logged_in'] = '0'
        session['cookies']['m_gdpr_mdl'] = '1'

    headers = headers or {}
    cookies = cookies or {}

    if 'cookie' not in headers:
        headers['cookie'] = ''
    req_cookies = headers['cookie'].split(';')
    req_cookies += [f'{k}={v}' for k, v in session['cookies'].items()]
    req_cookies += [f'{k}={v}' for k, v in cookies.items()]
    while '' in req_cookies:
        req_cookies.remove('')

    headers['cookie'] = '; '.join(req_cookies)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s = ssl.create_default_context().wrap_socket(s, server_hostname='myanimelist.net')
    s.connect(('myanimelist.net', 443))

    req = f'{method} {endpoint} HTTP/1.1\r\n'

    body = b''
    if data:
        body = urllib.parse.urlencode(data).encode()
        headers['Content-Type'] = 'application/x-www-form-urlencoded'
    elif json:
        body = __import__('json').dumps(json).encode()
        headers['Content-Type'] = 'application/json'

    headers['Content-Length'] = len(body)
    for i in headers:
        req += f'{i}: {headers[i]}\r\n'
    req += '\r\n'
    req = req.encode() + body + b'\r\n\r\n'

    s.send(req)

    buff = b''
    while not buff.endswith(b'\r\n\r\n'):
        buff += s.recv(1)
    headers = buff.strip().split(b'\r\n')
    response = int(headers[0].split(b' ')[1].decode()), headers[0].split(b' ')[2].decode()
    headers = HTTPHeaders(
        (i.split(b':', 1)[0].strip().decode().lower(), i.split(b':', 1)[1].strip().decode()) for i in headers[1:]
    )

    body = b''
    if headers.get('content-length') is not None:
        body = s.recv(int(headers.get('content-length')))
    else:
        if headers.get('transfer-encoding', '').lower() == 'chunked':
            while True:
                length = b''
                while not length.endswith(b'\r\n'):
                    length += s.recv(1)
                if len(length) == 2:
                    break
                length = int(length.strip().decode(), 16)
                if length == 0:
                    break
                read = b''
                while len(read) < length:
                    read += s.recv(min(8192, length - len(read)))
                body += read
                s.recv(2)
        else:
            raise ValueError('Unknown HTTP body')

    res = HTTPResponse(response, headers, body)

    if 'set-cookie' in res.headers:
        set_cookie(res.headers['set-cookie'])

    return res


def post(*args, **kwargs):
    return make_request('POST', *args, **kwargs)


def get(*args, **kwargs):
    return make_request('GET', *args, **kwargs)
