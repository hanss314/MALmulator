# MALmulator

## TODO

- [ ] Search Methods
  - [ ] Anime/Manga Search
- [x] Anime List Methods
  - [x] Add Anime
  - [x] Update Anime
  - [x] Delete Anime
  - [x] XML Values
- [ ] Manga List Methods
  - [ ] Add Manga
  - [ ] Update Manga
  - [ ] Delete Manga
  - [ ] XML Values
- [ ] Account Methods
  - [ ] Verify Credentials