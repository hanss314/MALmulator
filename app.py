from functools import wraps
import re
import os

from flask import Flask, request, session

from xml.etree import ElementTree

from animexml import TAGS
import reqs

app = Flask(__name__)
app.secret_key = os.urandom(16)

HTML_ERROR_RE = re.compile(r'<div class="badresult">[^\w<]*(?:<strong>)?\W*(.+?)\W*?(?:</strong>)?\W*</div>')
CSRF_RE = re.compile("<meta name='csrf_token' content='(.+?)'>")
NAME_RE = re.compile(r'<span itemprop="name">(.+?)</span>')

BASE_API = 'https://myanimelist.net'


def get_headers(auth=True):
    if auth:
        if 'csrf' not in session:
            get_csrf('/')

    headers = {
        'origin': 'https://myanimelist.net',
        'accept': '*/*',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'en-US,en;q=0.9',
        'Host': 'myanimelist.net',
        'authority': 'myanimelist.net',
        'x-requested-with': 'XMLHttpRequest',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 '
                      'Safari/537.36 '
    }

    return headers


def get_csrf(endpoint='/'):
    header = get_headers(False)

    r = reqs.get(f'{endpoint}', headers=header)
    csrf = CSRF_RE.findall(r.text)
    if csrf:
        session['csrf'] = csrf[0]
        print('SET CSRF TO ' + CSRF_RE.findall(r.text)[0] + ' FROM ' + endpoint)
    return r


def requires_auth(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if request.authorization is None:
            return 'No authentication provided', 401

        if 'cookies' not in session or 'login' not in session['cookies'] or 'sess' not in session['cookies'] or \
                'csrf' not in session:
            login()

        return func(*args, **kwargs)

    return decorated_function


def get_name_of(aid):
    r = reqs.get(f'/anime/{aid}', headers=get_headers())
    name = NAME_RE.findall(r.text)
    return name[0] if name else str(aid)


@app.route('/login', methods=['GET'])
def login():
    """Testing the login"""
    if request.authorization is None:
        return '', 204

    get_csrf('/login.php')

    headers = get_headers()
    data = {
        'user_name': request.authorization['username'],
        'password': request.authorization['password'],
        'csrf_token': session['csrf'],
        # I don't know if the following is needed but MAL seemed to be sending it
        'cookie': 1,
        'sublogin': 'Login',
        'submit': 1,
    }
    headers['accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
    headers['referer'] = 'https://myanimelist.net/login.php?from=%2F'

    r = reqs.post('/login.php?from=%2F', data=data, headers=headers)

    csrf = CSRF_RE.findall(r.text)
    if csrf:
        session['csrf'] = csrf[0]
        print(f'NEW CSRF: {csrf[0]}')

    login_error = HTML_ERROR_RE.findall(r.text)
    if login_error:
        success = False
        login_error = login_error[0]
    else:
        success = True
        login_error = ''

    print('DID LOGIN', login_error, success)

    return login_error, 200 if success else 401


@app.route('/api/animelist/update/<int:aid>.xml', methods=['POST'])
@requires_auth
def search(aid):
    """
    Allows the authenticating user to update an anime on their anime list.
    Formats: XML

    :param aid: Required. ID of the anime to update.
    :param data: Required. Contains anime values in XML format.

    :return: 'Updated' or detailed error message.
    """

    endpoint = f'/ownlist/anime/{aid}/edit?hideLayout'

    if 'data' not in request.form:
        return 'No data part provided in request form', 400

    data = ElementTree.fromstring(request.form.get('data'))

    header = get_headers()
    payload = {
        'anime_id': aid,
        'csrf_token': session['csrf'],
    }
    for element in data:
        if element.tag in TAGS:
            try:
                TAGS[element.tag](payload, element.text)
            except TypeError:
                return f'Failed to convert tag {element.tag}', 400

    header['content-type'] = 'application/x-www-form-urlencoded; charset=UTF-8'

    r = reqs.post(endpoint, data=payload, headers=header)

    error = HTML_ERROR_RE.findall(r.text)
    if error:
        return error[0], 400

    return 'Updated', 200


@app.route('/api/animelist/delete/<int:aid>.xml', methods=['POST', 'DELETE'])
@requires_auth
def delete(aid):
    """
    Allows the authenticating user to delete an anime from their anime list.
    Formats: XML

    :param aid: Required. ID of the anime to delete.

    :return: 'Deleted' or detailed error message.
    """

    endpoint = f'/ownlist/anime/{aid}/delete?hideLayout=1'

    header = get_headers()
    payload = {
        'csrf_token': session['csrf'],
    }
    header['content-type'] = 'application/x-www-form-urlencoded; charset=UTF-8'

    r = reqs.post(endpoint, data=payload, headers=header)

    error = HTML_ERROR_RE.findall(r.text)
    if error:
        return error[0], 400

    return 'Deleted', 200


@app.route('/api/animelist/add/<int:aid>.xml', methods=['POST'])
@requires_auth
def add(aid):
    """
    Allows the authenticating user to update an anime on their anime list.
    Formats: XML

    :param aid: Required. ID of the anime to update.
    :param data: Required. Contains anime values in XML format.

    :return: 'Updated' or detailed error message.
    """

    endpoint = f'/ownlist/anime/add'

    if 'data' not in request.form:
        return 'No data part provided in request form', 400

    data = ElementTree.fromstring(request.form.get('data'))

    header = get_headers()
    payload = {
        'anime_id': aid,
        'csrf_token': session['csrf'],
        'queryTitle': get_name_of(aid),
    }
    for element in data:
        if element.tag in TAGS:
            try:
                TAGS[element.tag](payload, element.text)
            except TypeError:
                return f'Failed to convert tag {element.tag}', 400

    header['content-type'] = 'application/x-www-form-urlencoded; charset=UTF-8'

    if 'status' in payload:
        payload['add_anime[status]'] = payload['status']
        del payload['status']
    else:
        payload['add_anime[status]'] = 1

    r = reqs.post(endpoint, data=payload, headers=header, cookies={'anime_update_advanced': '1'})

    error = HTML_ERROR_RE.findall(r.text)
    if error:
        return error[0], 400

    return 'Created', 201


if __name__ == '__main__':
    app.run(debug=True)
